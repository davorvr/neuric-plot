#!/usr/bin/python3

import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui
import numpy as np
import serial
import serial.tools.list_ports
import time
import threading
from collections import deque
import signal
signal.signal(signal.SIGINT, signal.SIG_DFL)

port = list(serial.tools.list_ports.grep("067b:2303"))
if not port:
	raise IOError("Serial device not found!")
else:
	dev = port[0][0]
baud = 19200
ser = serial.Serial(dev,baud)

pg.setConfigOptions(antialias=True)
pg.setConfigOption('background', (50, 50, 50))
pg.setConfigOption('foreground', (240, 240, 240))

app = QtGui.QApplication([])
view = pg.GraphicsView()
view.show()
l = pg.GraphicsLayout()
view.setCentralItem(l)
view.setWindowTitle('Membrane potential, real time')
view.resize(920,300)

p = l.addPlot()
p.setDownsampling(mode='peak')
p.setClipToView(True)
p.showAxis('right', show=True)
p.setLabel('bottom', 'Time (ms)')
p.setLabel('left', 'Potential (mV)')
p.showGrid(y=True, alpha=0.2)

p.setXRange(-10000, 0)
p.setYRange(-100, 55)
p.setLimits(xMax=0)
curve = p.plot()
curve.setPen(pg.mkPen(color=(240, 240, 240), width=2))

data = deque([], 350)
ptr = 0
t = QtCore.QTime()
t.start()
# ~ abc = 1.0


def update_ser():
    global data, ser
    # ~ global abc
    while True:
        #newpoint = float((float(ser.readline())/(2*32*4))-100)
        # ~ while len(ser_in) != 3:
            # ~ ser_in = ser.readline()
        ser_in = None
        while ser_in is None:
            try:
                ser_in = float(ser.readline())
            except ValueError:
                pass
        # ~ ser_in_no = ''.join(x for x in ser_in if x.isdigit())
        # ~ newpoint = float(int.from_bytes(ser_in[:-1], byteorder='big', signed=False))/(2*32*4)-100
        newpoint = float(ser_in)/(2*32*4)-100
        #newpoint = int(ser.readline())
        data.append({'x': t.elapsed(), 'y': newpoint})
        # ~ data.append({'x': abc, 'y': newpoint})
        # ~ abc += 1.0
        #curve.setData(x=x, y=y)

def update_plot():
    global x, y, p, data, curve
    data_temp = list(data)
    x = [item['x'] for item in data_temp]
    y = [item['y'] for item in data_temp]
    curve.setData(x=x, y=y)
    curve.setPos(-(t.elapsed()), 0)
    # ~ curve.setPos(-(abc), 0)

# ~ timer_ser = pg.QtCore.QTimer()
# ~ timer_ser.timeout.connect(update_ser)
# ~ timer_ser.start(0)

thread = threading.Thread(target=update_ser)
thread.start()

timer_plot = pg.QtCore.QTimer()
timer_plot.timeout.connect(update_plot)
timer_plot.start(10)

## Start Qt event loop unless running in interactive mode or using pyside.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
